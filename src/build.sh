#!/bin/bash
set -e

TMP="/tmp/eink-gnome"

rm -rf "$TMP"
rm -rf ../dist
mkdir -p "$TMP"/gnome-shell/assets

ASSETS="$TMP"/gnome-shell/assets

cp -r assets/* "$ASSETS"

(
    cd sass/
    sass --sourcemap=none gnome-shell-eink.scss "$ASSETS"/gnome-shell-eink.css
    # workaround for ubuntu gdm to use.
    cp "$ASSETS"/gnome-shell-eink.css "$ASSETS"/gdm3.css
    mv "$ASSETS"/gnome-shell-eink.css "$ASSETS"/gnome-shell.css
)

FILES=$(find "$ASSETS" -type f -printf "%P\n" | xargs -i echo "    <file>{}</file>")                         

cat > "$ASSETS"/gnome-shell-theme.gresource.xml <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<gresources>
  <gresource prefix="/org/gnome/shell/theme">
$FILES
  </gresource>
</gresources>
EOF

(
    cd "$ASSETS"
    glib-compile-resources gnome-shell-theme.gresource.xml
    rm gnome-shell-theme.gresource.xml
    mv gnome-shell-theme.gresource ../../
    mv gnome-shell.css ../
)

cat > "$TMP"/index.theme <<EOF
[Desktop Entry]
Type=X-GNOME-Metatheme
Name=EInk
Comment=High Contrast Theme used on e-ink devices like DASUNG paperlike
Encoding=UTF-8
EOF

cp -a "$TMP" ../dist
