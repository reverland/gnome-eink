PREFIX ?= /usr
IS_UBUNTU ?= $(shell [ "$$(lsb_release -si 2> /dev/null)" = Ubuntu ] && echo true)
PKGNAME = eink-gnome
MAINTAINER = reverland <sa@linuxer.me>

all: build
build:
	git submodule init
	git submodule update
	cd src && ./build.sh

clean:
	rm -rf dist/*

install:
	mkdir -p $(DESTDIR)$(PREFIX)/share/themes/EInk
	cp -a dist/gnome-shell/ $(DESTDIR)$(PREFIX)/share/themes/EInk/gnome-shell
	cp -a dist/index.theme $(DESTDIR)$(PREFIX)/share/themes/EInk/
	mkdir -p $(DESTDIR)$(PREFIX)/share/gnome-shell/modes
	cp -a src/modes/* $(DESTDIR)$(PREFIX)/share/gnome-shell/modes/
	mkdir -p $(DESTDIR)$(PREFIX)/share/gnome-shell/theme/EInk/
	cp -a dist/gnome-shell-theme.gresource $(DESTDIR)$(PREFIX)/share/gnome-shell/theme/EInk/
ifdef DESTDIR
else
ifeq ($(IS_UBUNTU), true)
	update-alternatives --install $(PREFIX)/share/gnome-shell/gdm3-theme.gresource gdm3-theme.gresource $(PREFIX)/share/gnome-shell/theme/EInk/gnome-shell-theme.gresource 20
else
	mv $(PREFIX)/share/gnome-shell/gnome-shell-theme.gresource $(PREFIX)/share/gnome-shell/gnome-shell-theme.gresource.old
	ln -sf $(PREFIX)/share/themes/EInk/gnome-shell-theme.gresource $(PREFIX)/share/gnome-shell/gnome-shell-theme.gresource
endif
endif


uninstall:
ifeq ($(IS_UBUNTU), true)
	update-alternatives --remove gdm3-theme.gresource $(PREFIX)/share/gnome-shell/theme/EInk/gnome-shell-theme.gresource 
	update-alternatives --auto gdm3-theme.gresource
else
	mv $(PREFIX)/share/gnome-shell/gnome-shell-theme.gresource.old $(PREFIX)/share/gnome-shell/gnome-shell-theme.gresource
endif
	rm -rf $(DESTDIR)$(PREFIX)/share/themes/EInk
	rm -rf $(DESTDIR)$(PREFIX)/share/gnome-shell/theme/EInk
	rm -rf $(PREFIX)/share/gnome-shell/modes/eink.json
